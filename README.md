# README #

### What is this repository for? ###

* Quick summary: A boiler plate for using Pygame and PyMunk (Physics).
* Version: 0.1
* Python: 3.4
* License: MIT (Pymunk and Pygame are subject to their own license terms.)

### Frameworks ###

* Pymunk: https://pymunk.readthedocs.org/en/latest/# v4.0
* Pygame: http://www.pygame.org/news.html v1.9.2a0

### How do I get set up? ###

* Dependencies are included in the folder. You can put those dependencies in the Python folder or keep them in the project folder.

### Who do I talk to? ###

* Repo owner or admin: holladaystudio (at) gmail.com